**APMG Umami test**
===============================

## General

Test project for APMG based on the Drupal Umami demo site.

### Demo Site

https://ampg.blackletter.london

Contact me for admin login details

### Installation

https://gitlab.com/JimiOBrien/apmg-drupal-test

The site is using sqlite for the database so no config imports are required.
There is no need to install the umami profile as the database is already set up.

Steps to install the site:

1. Clone the repository
2. Run `composer install` in the root directory of the project
3. DO NOT install the Umami profile as the database is already defined and the contents of `web/sites/default/` is included in the repo for demo purposes and an easy install
4. There is no need for `drush cim` because sqlite. However the config files do exist in `/config/sync` 
5. Run `drush cr` to clear the cache

### JSON API Endpont

There is a json api endpoint at `/lessons/lessons-json` which returns a list of lessons.

You can test this endpoint and the filters on it by visiting `/lessons/lessons-json-tester` this page lists all available filters on the lesson entity. There is a link to this tester page in the main menu for demo purposes.

### Lesson of the Day

Using Drupal alone is not really capable of accurately updating a daily lesson at midnight. So I have implemented a custom drush command to allow the updating of the daily lesson from an external source such as a server cron or an external service.

In order for the daily lesson to be updated at midnight a server cron job needs to be set up to run this custom drush command at midnight.

The custom drush command is `drush lessons:updateDailyLesson` and it will update the daily lesson. It has an alias of `drush updatedl`.

Currently the same functionality is being called from `hook_cron()` in the custom module `lessons` but this is not ideal as it will only run when a user visits the site and the cron runs. 

As a demo, manually running the cron from the admin area will change the daily lesson

The lesson of the day block can be found on the home page.

### The lesson entity

The lesson entities can be listed and filtered at `/lessons`. This page uses javascript to filter the lessons. 

In a production environment I would have used my JSON API end point and javascript or react to filter the lessons. But for this demo the javascript uses only the DOM to filter lessons. 

### Cart functionality

I haven't had time to implement a cart for the Lesson content entity. Because the lesson content entity is a node type, I would have avoided commerce based projects such as drupal/commerce and opted for a custom option instead.
I simply haven't had time to implement a custom cart and checkout process for this demo.

Having looked at options for a quick cart set up, this module looks most promising https://www.drupal.org/project/basic_cart, but I would prefer to build a custom module to handle carts and checkout.


