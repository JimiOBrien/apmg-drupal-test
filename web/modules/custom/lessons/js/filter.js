var filter = {
  container: null,
  lessonsContainer: null,
  elements: null,
  data: null,
  params: new URLSearchParams(window.location.search),
  init: function(container) {
    this.container = container;
    this.lessonsContainer = container.querySelector('.view-content');
    this.elements = this.getElements();
    if (this.elements.length) {
      this.data = this.getData();
    }
    this.buildFilters();
    this.doFilterOnInit();
  },
  getElements: function() {
    return this.lessonsContainer.querySelectorAll('.js-lesson');
  },
  getData: function() {
    const data = {
      'labels': new Set(),
      'recipeLabels': new Set(),
      'recipeCategories': new Set(),
    }
    this.elements.forEach((element) => {
      const elementParent = element.parentElement;
      // const computedStyle = window.getComputedStyle(elementParent);
      // const display = computedStyle.getPropertyValue('display');
      const display = elementParent.style.display;
      if (display !== 'none') {
        const label = element.getAttribute('data-label');
        const recipeLabels = element.getAttribute('data-recipe-labels');
        const recipeCategories = element.getAttribute('data-recipe-categories');
        if (label) {
          data.labels.add(label);
        }
        if (recipeLabels) {
          recipeLabels.split(',').map(label => label.trim()).forEach(data.recipeLabels.add, data.recipeLabels);
        }
        if (recipeCategories) {
          recipeCategories.split(',').map(label => label.trim()).forEach(data.recipeCategories.add, data.recipeCategories);
        }
      }
    });
    return data;
  },
  buildFilters: function() {
    if (!this.data) {
      return;
    }
    const filterContainer = document.createElement('div');
    filterContainer.classList.add('filters');

    const elementData = {
      labels: {
        label: 'Lesson title',
        options: Array.from(this.data.labels),
        selected: this.params.get('label'),
      },
      recipeLabels: {
        label: 'Contains recipes',
        options: Array.from(this.data.recipeLabels),
        selected: this.params.get('recipeLabel'),
      },
      recipeCategories: {
        label: 'Recipe Categories',
        options: Array.from(this.data.recipeCategories),
        selected: this.params.get('recipeCategory'),
      }
    }
    filterContainer.innerHTML = Object.entries(elementData).map(([key, value]) => {
      return `
        <div class="filter">
          <label for="${key}">${value.label}</label>
          <select id="${key}" class="filter-select">
            <option value="">All</option>
            ${value.options.map(label => `<option value="${label}" ${value.selected === label ? 'selected' : ''}>${label}</option>`).join('')}
          </select>
        </div>
      `;
    }).join('');
    this.container.prepend(filterContainer);
    this.container.querySelectorAll('.filter-select').forEach((select) => {
      select.addEventListener('change', this.filterOnSelect.bind(this));
    });

    const resetButton = document.createElement('div');
    resetButton.classList.add('reset-button');
    resetButton.innerHTML = 'Reset';
    resetButton.addEventListener('click', this.resetFilters.bind(this));
    filterContainer.appendChild(resetButton);
  },
  filterOnSelect: function() {
    const label = this.container.querySelector('#labels').value;
    const recipeLabel = this.container.querySelector('#recipeLabels').value;
    const recipeCategory = this.container.querySelector('#recipeCategories').value;
    this.elements.forEach((element) => {
      const labelValue = element.getAttribute('data-label');
      const recipeLabels = element.getAttribute('data-recipe-labels');
      const recipeCategories = element.getAttribute('data-recipe-categories');
      const elementParent = element.parentElement;
      if (
        (label && labelValue !== label) ||
        (recipeLabel && !recipeLabels.split(',').map(label => label.trim()).includes(recipeLabel)) ||
        (recipeCategory && !recipeCategories.split(',').map(label => label.trim()).includes(recipeCategory))
      ) {
        elementParent.style.display = 'none';
      }
      else {
        elementParent.style.display = 'flex';
      }
    });
    this.addUrlParams('label', label);
    this.addUrlParams('recipeLabel', recipeLabel);
    this.addUrlParams('recipeCategory', recipeCategory);
    this.rebuildFilters();
  },
  doFilterOnInit: function() {
    const label = this.container.querySelector('#labels').value;
    const recipeLabel = this.container.querySelector('#recipeLabels').value;
    const recipeCategory = this.container.querySelector('#recipeCategories').value;
    this.elements.forEach((element) => {
      const labelValue = element.getAttribute('data-label');
      const recipeLabels = element.getAttribute('data-recipe-labels');
      const recipeCategories = element.getAttribute('data-recipe-categories');
      const elementParent = element.parentElement;
      if (
        (label && labelValue !== label) ||
        (recipeLabel && !recipeLabels.split(',').map(label => label.trim()).includes(recipeLabel)) ||
        (recipeCategory && !recipeCategories.split(',').map(label => label.trim()).includes(recipeCategory))
      ) {
        elementParent.style.display = 'none';
      }
      else {
        elementParent.style.display = 'flex';
      }
    });
    //this.rebuildFilters();
  },
  rebuildFilters: function() {
    // this.elements = this.getElements();
    this.data = this.getData();
    this.container.querySelector('.filters').remove();
    this.buildFilters();
  },
  addUrlParams: function(key, value) {
    this.params.set(key, value);
    window.history.pushState({}, '', `?${this.params}`);
  },
  resetFilters: function() {
    this.params.delete('label');
    this.params.delete('recipeLabel');
    this.params.delete('recipeCategory');
    window.history.pushState({}, '', `?${this.params}`);
    this.rebuildFilters();
    this.filterOnSelect();

  }
}


const container = document.querySelector('.view-lessons');
if (container) {
  filter.init(container);
}