<?php

namespace Drupal\lessons\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Utility\Token;
use Drupal\lessons\LessonsManager;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush commandfile for lessons module.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class LessonsCommands extends DrushCommands {

  /**
   * Constructs a LessonsCommands object.
   */
  public function __construct(
    private readonly LessonsManager $lessonsManager,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lessons.manager'),
    );
  }

  /**
   * Update Daily lesson.
   *
   * @command lessons:updateDailyLesson
   * @usage lessons:updateDailyLesson
   * @aliases updatedl
   */
  public function updateDailyLesson(): void {
    $id = $this->lessonsManager->setDailyLesson();
    if($id) {
      $this->logger()->success('Daily lesson updateded to id: ' . $id);
    } else {
      $this->logger()->error('Error: Daily lesson not updated');
    }
  }

}
