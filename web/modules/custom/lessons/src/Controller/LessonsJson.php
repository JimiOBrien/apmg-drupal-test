<?php declare(strict_types=1);

namespace Drupal\lessons\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\lessons\LessonsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for APMG Lessons routes.
 */
final class LessonsJson extends ControllerBase {

  protected $lessonsManager;

  /**
   * The controller constructor.
   */
  public function __construct(LessonsManager $lessonsManager) {
    $this->lessonsManager = $lessonsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self($container->get('lessons.manager'));
  }

  /**
   * Builds the response.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function __invoke(): JsonResponse {
    $request = \Drupal::request();
    $params = $request->query->all();
    $lessons = $this->lessonsManager->getLessons($params);
    if ($lessons) {
      $lessonsArray = $this->lessonsManager->getLessonsArray($lessons);
      return new JsonResponse($lessonsArray);
    }
    return new JsonResponse(['error' => 'No lessons found']);
  }

}
