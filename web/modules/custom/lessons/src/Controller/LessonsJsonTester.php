<?php declare(strict_types = 1);

namespace Drupal\lessons\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\lessons\LessonsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Url;

/**
 * Returns responses for APMG Lessons routes.
 */
final class LessonsJsonTester extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly UrlGeneratorInterface $urlGenerator,
    private readonly LessonsManager $lessonsManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('url_generator'),
      $container->get('lessons.manager')
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $lessons = $this->lessonsManager->getLessons();

    if(!$lessons) {
      return ['#markup' => '<h2>There are no lessons in the system.</h2>'];
    }
    $lessonsArray = $this->lessonsManager->getLessonsArray($lessons);

    $build = [
      '#prefix' => '<div class="lessons-json-tester">',
      '#suffix' => '</div>',
    ];
    $build['message'] = [
      '#type' => 'item',
      '#prefix' => '<h2>',
      '#markup' => 'Use the links below to test the json api controller.',
      '#suffix' => '</h2>',
    ];
    $build['all'] = [
      '#type' => 'link',
      '#title' => 'All Lessons',
      '#url' => \Drupal\Core\Url::fromRoute('lessons.lessons_json'),
      '#attributes' => [
        'target' => '_blank',
        'class' => ['btn'],
      ],
    ];
    foreach ($lessonsArray as $lesson) {
      $categories = explode(',', $lesson['recipe_categories']);
      if($categories) {
        $build['categories']['header'] = [
          '#type' => 'item',
          '#markup' => '<h3>Categories</h3>',
        ];
        foreach ($categories as $category) {
          $category = trim($category);
          $build['categories'][$category] = [
            '#type' => 'link',
            '#title' => $category,
            '#url' => \Drupal\Core\Url::fromRoute('lessons.lessons_json', ['recipeCategory' => $category]),
            '#attributes' => [
              'target' => '_blank',
              'class' => ['btn'],
            ],
          ];
        }
      }
      $recipes = explode(',', $lesson['recipe_labels']);
      if($recipes) {
        $build['recipes']['header'] = [
          '#type' => 'item',
          '#markup' => '<h3>Recipes</h3>',
        ];
        foreach ($recipes as $recipe) {
          $recipe = trim($recipe);
          $build['recipes'][$recipe] = [
            '#type' => 'link',
            '#title' => $recipe,
            '#url' => \Drupal\Core\Url::fromRoute('lessons.lessons_json', ['recipeLabel' => $recipe]),
            '#attributes' => [
              'target' => '_blank',
              'class' => ['btn'],
            ],
          ];
        }
      }
    }
    dpm($build);
    return $build;
  }

}
