<?php declare(strict_types = 1);

namespace Drupal\lessons;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

/**
 * @todo Add class description.
 */
final class LessonsManager {

  /**
   * Constructs a LessonsManager object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Connection $database
  ) {}

  /**
   * @todo Add method description.
   */
  public function setDailyLesson(): string {

    $id = $this->configFactory->get('lessons.settings')->get('daily_lesson');

    $query = $this->entityTypeManager->getStorage('node')->getQuery()->condition('type', 'lesson')->accessCheck(true);
    $entityIds = $query->execute();
    $random = array_rand($entityIds);

    if($id) {
      while ($id == $entityIds[$random]) {
        $random = array_rand($entityIds);
      }
    }
    $this->configFactory->getEditable('lessons.settings')->set('daily_lesson', $entityIds[$random])->save();
    return $entityIds[$random];
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDailyLesson() {
    $id = $this->configFactory->get('lessons.settings')->get('daily_lesson');
    if(!$id) {
      $id = $this->setDailyLesson();
    }
    $entity = $this->entityTypeManager->getStorage('node')->load($id);
    return $entity;
  }

  public function getLessons($params = null):array|bool {

    $results = [];
    $entities = [];

    $query = $this->database->select('node_field_data', 'n')
      ->fields('n', ['nid'])
      ->condition('type', 'lesson')
      ->condition('status', 1);

    if($params) {
//      if(isset($params['label']) && $params['label'] != '') {
//        $escapeTitle = $this->database->escapeLike($params['label']);
//        $query->condition('n.title', '%' . $escapeTitle . '%', 'LIKE');
//      }
      if(isset($params['recipeLabel']) && $params['recipeLabel'] != '') {
        $escapeLabel = $this->database->escapeLike($params['recipeLabel']);
        $query->join('node__field_recipe_labels', 'l', 'n.nid = l.entity_id');
        $query->condition('l.field_recipe_labels_value', '%' . $escapeLabel . '%', 'LIKE');
      }
      if(isset($params['recipeCategory']) && $params['recipeCategory'] != '') {
        $escapeCategory = $this->database->escapeLike($params['recipeCategory']);
        $query->join('node__field_recipe_categories', 'c', 'n.nid = c.entity_id');
        $query->condition('c.field_recipe_categories_value', '%' . $escapeCategory . '%', 'LIKE');
      }
    }
    $result = $query->execute();
    if($result) {
      $results = $result->fetchCol();
    }
    if($results) {
      return Node::loadMultiple($results);
    }
    return false;
  }

  public function getLessonsArray(array $lessons):array {

    $lessonsArray = [];

    /** @var \Drupal\node\Entity\Node $lesson */
    foreach ($lessons as $lesson) {
      $lessonsArray[$lesson->id()] = [
        'title' => $lesson->getTitle(),
        'url' => $lesson->toUrl('canonical', ['absolute' => TRUE])->toString(),
        'recipe_labels' => $lesson->get('field_recipe_labels')->value,
        'recipe_categories' => $lesson->get('field_recipe_categories')->value,
        'body' => $lesson->get('body')->value,
      ];
      $related_recipes = $lesson->get('field_related_recipes')->referencedEntities();
      foreach ($related_recipes as $related_recipe) {
        $lessonsArray[$lesson->id()]['related_recipes'][$related_recipe->id()] = [
          'title' => $related_recipe->getTitle(),
          'url' => $related_recipe->toUrl('canonical', ['absolute' => TRUE])->toString(),
        ];
      }
      $media_entities = $lesson->get('field_image')->referencedEntities();
      if($media_entities) {
        /** @var \Drupal\media\Entity\Media $media */
        $media = reset($media_entities);
        $fileRef = $media->get('field_media_image')->first()->getValue();
        $file = File::load($fileRef['target_id']);
        $lessonsArray[$lesson->id()]['image'] = $file->createFileUrl(false);
      }
    }
    return $lessonsArray;
  }
  public function getRecipes():array|bool {
    $results = [];
    $entities = [];

    $query = $this->database->select('node_field_data', 'n')
      ->fields('n', ['nid'])
      ->condition('type', 'recipe');

    $result = $query->execute();
    if($result) {
      $results = $result->fetchCol();
    }
    if($results) {
      return Node::loadMultiple($results);
    }
    return false;
  }

}
