<?php declare(strict_types = 1);

namespace Drupal\lessons\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\lessons\LessonsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a lesson of the day block.
 *
 * @Block(
 *   id = "lessons_daily_lesson",
 *   admin_label = @Translation("Lesson of the day"),
 *   category = @Translation("Custom"),
 * )
 */
final class DailyLessonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly LessonsManager $lessonsManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lessons.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $entity = $this->lessonsManager->getDailyLesson();
    $build['content'] = [
      '#cache' => [
        'max-age' => 0,
      ],
      '#theme' => 'custom__block__lessons__daily_lesson',
      '#lesson' => $entity,
    ];
    return $build;
  }

}
